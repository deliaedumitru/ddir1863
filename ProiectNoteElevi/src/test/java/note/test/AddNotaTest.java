package note.test;

import note.controller.NoteController;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AddNotaTest {
	
	private NoteController ctrl;
	
	@Before
	public void init(){
		ctrl = new NoteController();
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test
	public void test1() throws ClasaException {
		/**
		 * TC1 EC
		 */
		Nota nota = new Nota(1, "matematica", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
		assertEquals(ctrl.getNote().get(ctrl.getNote().size() - 1), nota);
	}
	
	@Test
	public void test2() throws ClasaException {
		/**
		 * TC3 EC
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1200, "matematica", 8);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test3() throws ClasaException {
		/**
		 * TC6 EC
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.nonexistentMaterie);
		Nota nota = new Nota(1, "calcul numeric", 8);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test4() throws ClasaException {
		/**
		 * TC7 EC
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "matematicaaaaaaaaaaaaa", 8);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test5() throws ClasaException {
		/**
		 * TC10 EC
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "matematica", 11);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test6() throws ClasaException {
		/**
		 * TC3 BVA
		 */
		Nota nota = new Nota(1000, "matematica", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
		assertEquals(ctrl.getNote().get(ctrl.getNote().size() - 1), nota);
	}

	@Test
	public void test7() throws ClasaException {
		/**
		 * TC5 BVA
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(0, "matematica", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test8() throws ClasaException {
		/**
		 * TC11 BVA
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.nonexistentMaterie);
		Nota nota = new Nota(1, "matematicaaaaaaaaaaa", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test9() throws ClasaException {
		/**
		 * TC15 BVA
		 */
		Nota nota = new Nota(1, "sport", 1);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
		assertEquals(ctrl.getNote().get(ctrl.getNote().size() - 1), nota);
	}

	@Test
	public void test10() throws ClasaException {
		/**
		 * TC17 BVA
		 */
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "sport", 0);
		ctrl.addNota(nota);
	}
}
