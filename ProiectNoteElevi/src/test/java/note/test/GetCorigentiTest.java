package note.test;

import static org.junit.Assert.*;

import java.util.List;

import note.model.Corigent;
import note.model.Elev;
import note.model.Nota;

import org.junit.Before;
import org.junit.Test;

import note.utils.ClasaException;

import note.controller.NoteController;


public class GetCorigentiTest {

	private NoteController ctrl;

	@Before
	public void setUp() throws Exception {
		ctrl = new NoteController();
	}

	@Test
	public void test1() throws ClasaException {
		Elev e1 = new Elev(1, "gigi");
		Elev e2 = new Elev(2, "gigelle");
		ctrl.addElev(e1);
		ctrl.addElev(e2);
		Nota n1 = new Nota(1,"romana", 10);
		Nota n2 = new Nota(1,"romana", 7);
		Nota n3 = new Nota(1,"matematica", 10);
		Nota n4 = new Nota(1,"matematica", 10);
		Nota n5 = new Nota(2,"matematica", 4);
		Nota n6 = new Nota(2,"matematica", 5);
		Nota n7 = new Nota(2,"matematica", 6);
		Nota n8 = new Nota(2,"romana", 7);
		ctrl.addNota(n1);
		ctrl.addNota(n2);
		ctrl.addNota(n3);
		ctrl.addNota(n4);
		ctrl.addNota(n5);
		ctrl.addNota(n6);
		ctrl.addNota(n7);
		ctrl.addNota(n8);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
		assertEquals(corigenti.size(),0);
	}


	@Test
	public void test2() throws ClasaException {
		Elev e1 = new Elev(1, "gigi");
		Elev e2 = new Elev(2, "gigelle");
		ctrl.addElev(e1);
		ctrl.addElev(e2);
		Nota n1 = new Nota(1,"romana", 10);
		Nota n2 = new Nota(1,"romana", 7);
		Nota n3 = new Nota(1,"matematica", 10);
		Nota n4 = new Nota(1,"matematica", 10);
		Nota n5 = new Nota(2,"matematica", 4);
		Nota n6 = new Nota(2,"matematica", 5);
		Nota n7 = new Nota(2,"matematica", 3);
		Nota n8 = new Nota(2,"romana", 7);
		ctrl.addNota(n1);
		ctrl.addNota(n2);
		ctrl.addNota(n3);
		ctrl.addNota(n4);
		ctrl.addNota(n5);
		ctrl.addNota(n6);
		ctrl.addNota(n7);
		ctrl.addNota(n8);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
		assertEquals(corigenti.size(),1);
	}

	@Test
	public void test3() throws ClasaException {
		Elev e1 = new Elev(1, "gigi");
		Elev e2 = new Elev(2, "gigelle");
		ctrl.addElev(e1);
		ctrl.addElev(e2);
		Nota n1 = new Nota(1,"romana", 1);
		Nota n2 = new Nota(1,"romana", 7);
		Nota n3 = new Nota(1,"matematica", 10);
		Nota n4 = new Nota(1,"matematica", 10);
		Nota n5 = new Nota(2,"matematica", 4);
		Nota n6 = new Nota(2,"matematica", 5);
		Nota n7 = new Nota(2,"matematica", 3);
		Nota n8 = new Nota(2,"romana", 7);
		ctrl.addNota(n1);
		ctrl.addNota(n2);
		ctrl.addNota(n3);
		ctrl.addNota(n4);
		ctrl.addNota(n5);
		ctrl.addNota(n6);
		ctrl.addNota(n7);
		ctrl.addNota(n8);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
		assertEquals(corigenti.get(1).getNrMaterii(), corigenti.get(0).getNrMaterii());
	}

	@Test
	public void test4() throws ClasaException {
		Elev e1 = new Elev(1, "gigi");
		Elev e2 = new Elev(2, "gigelle");
		ctrl.addElev(e1);
		ctrl.addElev(e2);
		Nota n1 = new Nota(1,"romana", 1);
		Nota n2 = new Nota(1,"romana", 7);
		Nota n3 = new Nota(1,"matematica", 10);
		Nota n4 = new Nota(1,"matematica", 10);
		Nota n5 = new Nota(2,"matematica", 4);
		Nota n6 = new Nota(2,"matematica", 5);
		Nota n7 = new Nota(2,"matematica", 3);
		Nota n8 = new Nota(2,"romana", 4);
		ctrl.addNota(n1);
		ctrl.addNota(n2);
		ctrl.addNota(n3);
		ctrl.addNota(n4);
		ctrl.addNota(n5);
		ctrl.addNota(n6);
		ctrl.addNota(n7);
		ctrl.addNota(n8);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
//		assertEquals(corigenti.get(1).getNrMaterii()+1, corigenti.get(0).getNrMaterii());
	}

	@Test
	public void test5() throws ClasaException {
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
		assertEquals(corigenti.size(),0);
	}

	@Test
	public void test6() throws ClasaException {
		//Elev e1 = new Elev(1, "gigi");
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Corigent> corigenti = ctrl.getCorigenti();
		assertEquals(corigenti.size(),0);
	}
}
