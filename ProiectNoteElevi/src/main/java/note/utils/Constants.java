package note.utils;

public class Constants {
	public static int minNota = 1;
	public static int maxNota = 10;
	public static int minNrmatricol = 1;
	public static int maxNrmatricol = 1000;
	public static String invalidNota = "Nota introdusa nu este corecta";
	public static String invalidMateria = "Lungimea materiei este invalida";
	public static String invalidNrmatricol = "Numarul matricol introdus nu este corect";
	public static String nonexistentNrmatricol = "Nu exista niciun elev cu acest numar matricol.";
	public static String emptyRepository = "Nu exista date";
	public static String nonexistentMaterie = "Materia nu exista";
}
