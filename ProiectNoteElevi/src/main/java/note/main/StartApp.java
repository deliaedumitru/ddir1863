package note.main;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

//functionalitati
//i.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata); 
//ii.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol), 
//iii.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException 
	 */
	public static void main(String[] args) throws ClasaException {
		// TODO Auto-generated method stub
		NoteController ctrl = new NoteController();
		List<Medie> medii = new LinkedList<Medie>();
		List<Corigent> corigenti = new ArrayList<Corigent>();
		ctrl.readElevi(args[0]);
		ctrl.readNote(args[1]);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		boolean gasit = false;
		while(!gasit) {
			System.out.println("1. Adaugare Nota");
			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
				case 1:
					try {
						Scanner keyboard = new Scanner(System.in);
						System.out.println("Introduceti numarul matricol: ");
						int nrmatricol = keyboard.nextInt();
						List<Elev> elevi = ctrl.getElevi();
						boolean exists = false;
						for (Elev e: elevi
							 ) {
							if(e.getNrmatricol() == nrmatricol) {
								exists = true;
							}
						}
						if(exists == false) {
							throw new ClasaException(Constants.nonexistentNrmatricol);
						}
						System.out.println("Introduceti materia: ");
						String materie = keyboard.next();
						System.out.println("Introduceti nota: ");
						double nota = keyboard.nextDouble();
						ctrl.addNota(new Nota(nrmatricol, materie, nota));
						List<Nota> note = ctrl.getNote();
						for (Nota n: note
								) {
							System.out.println(n.toString());
						}
						ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
						System.out.println(ctrl.getClasa());
					} catch (ClasaException e) {
						System.out.println(e.getMessage());
					}

				case 2: medii = ctrl.calculeazaMedii();
						for(Medie medie:medii)
							System.out.println(medie);
					ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
					System.out.println(ctrl.getClasa());
						break;
				case 3: corigenti = ctrl.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
						System.out.println(ctrl.getClasa());
						break;
				case 4: gasit = true;
						break;
				default: System.out.println("Introduceti o optiune valida!");
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
